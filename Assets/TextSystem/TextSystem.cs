﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSystem : MonoBehaviour {
	public GameObject PF_MESSAGE_OBJECT;

	public Transform messageQueueContainer;
	private List<Transform> messageQueueMessages = new List<Transform>();
	public int messageQueueMaxLength;

	public Transform messageQueueHistoryContainer;
	private List<Transform> messageQueueHistoryMessages = new List<Transform>();
	private List<Message> messageQueueHistory = new List<Message>();
	public int messageQueueHistoryMaxLength;

	public void Enqueue(Message newMessage){
		//Add new message to rendered messages showing
		RenderNewMessage(newMessage);		
		//clear excess messages
		while(messageQueueMaxLength != 0 && messageQueueMessages.Count > messageQueueMaxLength){
			Destroy(messageQueueMessages[0].gameObject);
			messageQueueMessages.RemoveAt(0);
		}

		//Add message to unrendered history queue
		messageQueueHistory.Add(newMessage);
		//clear excess history queue
		while(messageQueueHistoryMaxLength != 0 && messageQueueHistory.Count > messageQueueHistoryMaxLength){
			messageQueueHistoryMessages.RemoveAt(0);
		}
	}
	
	private void RenderNewMessage(Message newMessage) {
		Transform newMessageObject = Instantiate(PF_MESSAGE_OBJECT).transform;
		newMessageObject.SetParent(messageQueueContainer);

		//Assign appropriate colors to message
		newMessageObject.GetChild(0).GetComponent<Text>().color = newMessage.senderColor;
		newMessageObject.GetChild(1).GetComponent<Text>().color = newMessage.messageColor;

		float senderNameSpaceBuffer = 0;
		if((object)newMessage.senderName != null && newMessage.senderName != ""){
			newMessageObject.GetChild(0).GetComponent<Text>().text = newMessage.senderName + ":";
			senderNameSpaceBuffer = 16;
		}
		newMessageObject.GetChild(1).GetComponent<Text>().text = newMessage.message;

		//Assign height width
		newMessageObject.GetComponent<RectTransform>().sizeDelta = new Vector2(
			messageQueueContainer.GetComponent<RectTransform>().sizeDelta.x,
			0
		);
		//Must recalculate UI after adjusting width to get correct height needed
		newMessageObject.GetComponent<RectTransform>().sizeDelta = new Vector2(
			0,
			newMessageObject.GetChild(1).GetComponent<Text>().preferredHeight + 16
		);

		//Assign position as bottom of textsystem
		newMessageObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(
			0,
			(0 - this.GetComponent<RectTransform>().sizeDelta.y / 2) + 
				((newMessageObject.GetChild(1).GetComponent<Text>().preferredHeight + 16) / 2),
			0
		);

		//Move every other message upwards
		messageQueueMessages.ForEach(delegate(Transform message){
			message.GetComponent<RectTransform>().anchoredPosition = new Vector2(
				message.GetComponent<RectTransform>().anchoredPosition.x,
				message.GetComponent<RectTransform>().anchoredPosition.y + 
					(newMessageObject.GetChild(1).GetComponent<Text>().preferredHeight + 
					senderNameSpaceBuffer) + 
					3//Buffer space between messages
			);
		});

		//Add the message to the message list
		messageQueueMessages.Add(newMessageObject);
	}

	public void ShowHistory(){
		//History queue rendered on button to save performance each enqueue
		messageQueueContainer.gameObject.SetActive(false);
		messageQueueHistoryContainer.gameObject.SetActive(true);
	}

	public void HideHistory(){
		//History queue unrendered on button to unclog gameobject bloat
		messageQueueContainer.gameObject.SetActive(true);
		messageQueueHistoryContainer.gameObject.SetActive(false);
	}
}