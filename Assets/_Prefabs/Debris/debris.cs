﻿using UnityEngine;
using UnityEngine.Networking;

public class debris : NetworkBehaviour {
	[SyncVar]
	public Vector2 scale;

	public void Start(){
		this.transform.localScale = new Vector3(
			scale.x,
			scale.y,
			1f
		);

		if (!isServer){
			this.transform.parent = ClientGlobals.instance.debrisContainer;
		}
	}
}