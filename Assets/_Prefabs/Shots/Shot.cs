﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shot : NetworkBehaviour {
	public PlayerInput owner;

	float destroyTime;
	public float duration;
	public int damage;

	public float bulletSize;

	void Start(){
		bulletSize = this.GetComponent<BoxCollider2D>().size.x * this.transform.localScale.x * 2f;
		destroyTime = Time.time + duration;

		this.transform.position += new Vector3(
			Mathf.Cos(Mathf.Deg2Rad * (this.transform.rotation.eulerAngles.z + 90)) * bulletSize,
			Mathf.Sin(Mathf.Deg2Rad * (this.transform.rotation.eulerAngles.z + 90)) * bulletSize,
			0
		);
	}

	public void init(float vel, PlayerInput p_owner){		
		this.GetComponent<Rigidbody2D>().velocity = new Vector2(
			Mathf.Cos(Mathf.Deg2Rad * (this.transform.rotation.eulerAngles.z + 90)) * vel,
			Mathf.Sin(Mathf.Deg2Rad * (this.transform.rotation.eulerAngles.z + 90)) * vel
		);

		owner = p_owner;
	}

	void Update () {
		if (Time.time >= destroyTime){
			Destroy(this.gameObject);
			Explode();
		}
	}

	protected virtual void Explode(){;}

	void OnCollisionEnter2D(Collision2D other){
		if (!isServer)
			Destroy(this.gameObject);
		else{
			other.gameObject.SendMessage("Damage", new DamageClass(damage, owner), SendMessageOptions.DontRequireReceiver);
			Explode();
			Destroy(this.gameObject);
		}
	}
}
