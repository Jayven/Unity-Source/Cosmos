﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Weapon : MonoBehaviour {
	public Ship owner;

	public float fireRate;
	public GameObject PF_shot;
	float lastFire;
	public float fuelCost;

	public float shotVelocity;

	public void Fire() {
		if (lastFire + fireRate < Time.time){
			lastFire = Time.time;
			FireNow();
		}
	}

	protected virtual void FireNow(){
		GameObject newShot = Instantiate(PF_shot, this.transform.position , this.transform.rotation) as GameObject;
		newShot.GetComponent<Shot>().init(shotVelocity, owner.player);
        CmdCreateShot(newShot);
	}

	void CmdCreateShot(GameObject newShot){
        NetworkServer.Spawn(newShot);
	}
}