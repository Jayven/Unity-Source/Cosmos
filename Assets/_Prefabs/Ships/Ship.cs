﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Ship : NetworkBehaviour {
	public bool isPlayer = true;//Determines if this entity is a real player or computer

	public Sprite sprite;

	public int cost;//How much does the ship cost to buy in shop
	public string shipName;//Default ship name in shop

	[SyncVar]
	public String playerName;

	public PlayerInput player;

	public GameObject mapIcon;

	public Weapon[] SCRIPT_weapons1;
	public Weapon[] SCRIPT_weapons2;
	public Weapon[] SCRIPT_weapons3;

	public Engine[] SCRIPT_mainEngines;
	public Engine[] SCRIPT_leftEngines;
	public Engine[] SCRIPT_rightEngines;

	public Rigidbody2D shipBody;

	[SyncVar]
	public float fuel;
	public int maxFuel;
	[SyncVar(hook = "HealthChanged")]
	public float health;
	public int maxHealth;

	[SyncVar]
	public Color defaultColor = Color.white;

	public float thrust;
	[SyncVar]
	public float percentThrust;//Percent of max thrust we are using
	public float maxSpeed;
	public float torque;
	[SyncVar]
	public float percentTorque;//Percent of max torque we are using //range(-1, 1)

	#if !SERVER
	public Transform PF_UI;
	private Transform UI;
	private Text UINamePlate;
	private Text UIHealthText;
	private GameObject UIHealthBar;
	private Vector3 UIOffset;
	#endif

	void Start(){
		health = maxHealth;
		fuel = maxFuel;

		#if !SERVER
		UI = Instantiate(PF_UI, this.transform.position, Quaternion.Euler(new Vector3(0,0,0))) as Transform;
		UINamePlate = UI.GetChild(0).GetComponent<Text>();
		UIHealthText = UI.GetChild(1).GetChild(2).GetComponent<Text>();
		UIHealthBar = UI.GetChild(1).GetChild(1).gameObject;
		UIOffset = new Vector3(0, 1.5f, 0);

		this.transform.GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = defaultColor;

		UINamePlate.text = playerName;

		HealthChanged(health);
		#endif
	}

	[Server]
	public void init(PlayerInput myPlayer){
		if(myPlayer != null){
			playerName = myPlayer.username;

			ServerGlobals.instance.playerShips.Add(this);

			NetworkServer.Spawn(this.gameObject);

			player = myPlayer;

			ServerGlobals.instance.Log(playerName + " spawned a " + shipName);
			ServerGlobals.instance.Log(ServerGlobals.instance.playerShips.Count.ToString() + " ships Alive");
		}
		else{
			defaultColor = Color.red;

			isPlayer = false;
			playerName = "COM";
			ServerGlobals.instance.cpuShips.Add(this);
			StartCoroutine(AIManager());
			NetworkServer.Spawn(this.gameObject);


			ServerGlobals.instance.Log("Server spawned a " + shipName);
			ServerGlobals.instance.Log(ServerGlobals.instance.cpuShips.Count.ToString() + " computers Alive");
		}
		
	}

	void FixedUpdate(){
		//Propell ship
		if (percentThrust > 0 && fuel > 0){
			shipBody.AddForce(transform.up * (percentThrust * thrust));
		}

		//enforce maximum velocity
		shipBody.velocity = new Vector2(
			Mathf.Clamp(shipBody.velocity.x, 0 - maxSpeed, maxSpeed),
			shipBody.velocity.y
		);
		//Rotate ship
		if (fuel > 0){
			shipBody.AddTorque(percentTorque * torque);
		}

		if(isServer){
			if (fuel <= 0){
				this.Damage(new DamageClass(Time.deltaTime / 8f, player));
			}
		}
		else{
			UpdateEngineDecals();
		}
	}

	void Update(){
		if(!isServer){
			#if !SERVER
			UI.transform.position = this.transform.position + UIOffset;
			#endif
		}
	}

	[Client]
	void UpdateEngineDecals(){
		//Update forward engine decals
		foreach (Engine engine in SCRIPT_mainEngines){
			if (Power(engine.fuelCost * (Mathf.Max(percentThrust, 0) * thrust))){//Use engine cost to reduce ship fuel
				engine.Throttle(Mathf.Max(percentThrust, 0) * thrust);
			}
			else{
				engine.Throttle(0);
			}
		}

		//Update left engine decals
		foreach (Engine engine in SCRIPT_leftEngines){
			if (Power(engine.fuelCost * (Mathf.Max(-percentTorque, 0) * torque))){//Use engine cost to reduce ship fuel
				engine.Throttle( - Mathf.Min(percentTorque * torque, 0));
			}
			else{
				engine.Throttle(0);
			}
		}

		//Update right engine decals
		foreach (Engine engine in SCRIPT_rightEngines){

			if (Power(engine.fuelCost * (Mathf.Max(percentTorque, 0) * torque))){//Use engine cost to reduce ship fuel
				engine.Throttle(Mathf.Max(percentTorque * torque, 0));
			}
			else{
				engine.Throttle(0);
			}
		}
	}

	[Server]
	public virtual void Fire1(){
		foreach (Weapon weapon in SCRIPT_weapons1){
			if (Power(weapon.fuelCost)){
				weapon.Fire();
			}
		}
	}

	[Server]
	public virtual void Fire2(){
		foreach (Weapon weapon in SCRIPT_weapons2){
			if (Power(weapon.fuelCost)){
				weapon.Fire();
			}
		}
	}

	[Server]
	public virtual void Fire3(){
		foreach (Weapon weapon in SCRIPT_weapons3){
			if (Power(weapon.fuelCost)){
				weapon.Fire();
			}
		}
		Damage(new DamageClass(1f, player));
	}

	public void Forward(float p_moveVertical){
		percentThrust = Mathf.Clamp(p_moveVertical, 0, 1);
	}

	public void Tilt(float p_moveHorizontal){
		percentTorque = Mathf.Clamp(p_moveHorizontal, -1, 1);
	}

	[Server]
	public void Damage(DamageClass damager){
		if (isServer){
			health -= damager.damage;
			if (health <= 0){
				if (damager.damager != null){
					damager.damager.SendMessage("KilledSomeone", this.playerName, SendMessageOptions.DontRequireReceiver);
					this.player.SendMessage("RpcDied", damager.damager.username, SendMessageOptions.DontRequireReceiver);
				}
				Kill();
			}
		}
		else{
			#if !SERVER
			UIHealthText.text = "Health: " + health.ToString();
			UIHealthBar.GetComponent<RectTransform>().anchoredPosition = new Vector3(
				- (health / maxHealth) - 1,
				UIHealthBar.GetComponent<RectTransform>().anchoredPosition.y,
				0
			);
			#endif
		}
	}

	void HealthChanged(float tmp){
		if(!isServer){
			health = tmp;
			#if !SERVER
			UIHealthText.text = "Health: " + tmp.ToString();
			UIHealthBar.GetComponent<RectTransform>().anchoredPosition = new Vector3(
				(tmp / maxHealth) - 1,
				UIHealthBar.GetComponent<RectTransform>().anchoredPosition.y,
				0
			);
			#endif
		}
	}


	[Server]
	virtual public void Kill(){
		Destroy(this.gameObject);
		if(isPlayer){
			player.ship = null;
			player.SCRIPT_ship = null;
		}
	}

	public void OnDestroy(){
		if (isServer){
			if (player != null){
				player.RpcDeassignShip();
			}
			if (isPlayer){
				ServerGlobals.instance.playerShips.Remove(this);
			}
			else{
				ServerGlobals.instance.cpuShips.Remove(this);
			}
		}
		#if !SERVER
		if (UI){
			Destroy(UI.gameObject);
		}
		#endif
		
	}

	bool Power(float cost){
		if (fuel >= cost){
			fuel -= cost;
			return true;
		}
		else{
			fuel = 0;
			return false;
		}
	}

	public float[] Polar(){
		float radius = Mathf.Sqrt(Mathf.Pow(this.transform.position.x, 2) + Mathf.Pow(this.transform.position.y, 2));
		float theta = Mathf.Atan(this.transform.position.y / this.transform.position.x);

		theta /= Mathf.PI;

		if (this.transform.position.x < 0){
			theta ++;
		}
		else if (this.transform.position.y < 0){
			theta ++;
			theta ++;
		}

		return new float[2]{radius, theta};
	}

	[Server]
	//This function overrides any input and instead implements a self contained AI to pilot the ship
	//If isPlayer is changed after this routine starts, entity reverts to accepting player input and kills AI
	IEnumerator AIManager(){
		const float PLAYER_PRIORITY = 1.5f;//How many times farther than a player be and still get targeted compared to a cpu

		isPlayer = false;
		WaitForSeconds delay = new WaitForSeconds(.1f);
		WaitForSeconds longDelay = new WaitForSeconds(1.5f);
		WaitForSeconds veryLongDelay = new WaitForSeconds(10f);

		while (true){
			GameObject target = null;
			float targetDistance = 100f;//This creates the max distance it will pursue

			//Find my target
			foreach(Ship ship in ServerGlobals.instance.cpuShips){
				if (!GameObject.ReferenceEquals(this.gameObject, ship.gameObject)){
					//Uses cart distance instead of pythagorian to avoid multiplication performance hit
					//Creates max error of (2-sqrt(2)N)
					float distance = Mathf.Abs(ship.gameObject.transform.position.x - this.gameObject.transform.position.x) +
						Mathf.Abs(ship.gameObject.transform.position.y - this.gameObject.transform.position.y);

					if (distance < targetDistance){
						target = ship.gameObject;
						targetDistance = distance * PLAYER_PRIORITY;
					}					
				}
			}
			foreach(Ship ship in ServerGlobals.instance.playerShips){
				if (!GameObject.ReferenceEquals(this.gameObject, ship.gameObject)){
					//Uses cart distance instead of pythagorian to avoid multiplication performance hit
					//Creates max error of (2-sqrt(2)N)
					float distance = Mathf.Abs(ship.gameObject.transform.position.x - this.gameObject.transform.position.x) +
						Mathf.Abs(ship.gameObject.transform.position.y - this.gameObject.transform.position.y);

					if (distance < targetDistance){
						target = ship.gameObject;
						targetDistance = distance;
					}					
				}
			}

			if (target != null){
				//Turn to target

				float deltaX = target.gameObject.transform.position.x - this.gameObject.transform.position.x;
				float deltaY = target.gameObject.transform.position.y - this.gameObject.transform.position.y;


				float targetTheta;//Theta to get to target, not targets rotation

				if(deltaY > 0){
					targetTheta = -Mathf.Atan(deltaX / deltaY) *180/Mathf.PI;
				}
				else{
					targetTheta = Mathf.Sign(-deltaX)*180-(Mathf.Atan(deltaX / deltaY) *180/Mathf.PI);
				}

				float deltaTheta = targetTheta - this.gameObject.transform.rotation.eulerAngles.z;

				while(deltaTheta > 180 || deltaTheta < - 180){
					deltaTheta -= Mathf.Sign(deltaTheta) * 360;
				}

				if (float.IsNaN(deltaTheta)){
					percentTorque = 0;
				}
				else{
					percentTorque = deltaTheta / 180;
				}

				//Shoot?
				if (targetDistance < 15f){
					if (UnityEngine.Random.value < .1){
						this.Fire1();
					}
					if (UnityEngine.Random.value < .1){
						this.Fire2();
					}
					if (UnityEngine.Random.value < .1){
						this.Fire3();
					}
				}
				else{
					yield return longDelay;//We'll pause longer in non combat situations
				}


				//Suicidally march forward
				percentThrust = 1f;
			}
			else{
				percentThrust = 0f;
				percentTorque = 0f;
				yield return veryLongDelay;//We'll pause very long when no enemy is near
			}

			yield return delay;

			if (isPlayer){
				ServerGlobals.instance.cpuShips.Remove(this);
				ServerGlobals.instance.playerShips.Add(this);
				break;
			}
		}

	}
}