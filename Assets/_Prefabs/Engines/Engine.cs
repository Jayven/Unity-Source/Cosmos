﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Engine : MonoBehaviour {
	public float fuelCost;
	public float K;//Controls all constants regarding engine's size

	public void Throttle(float size){
		this.transform.GetChild(0).localScale = new Vector3(1, K * .05f * size, 1f);
		this.transform.GetChild(1).localScale = new Vector3(1, K * .1f * size, 1f);
	}
}
