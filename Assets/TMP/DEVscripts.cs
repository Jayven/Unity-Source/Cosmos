﻿#if !(SERVER || CLIENT)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DEVscripts : MonoBehaviour {
	public Camera mainCamera;

	public void ChangeViewSize(Scrollbar slider){
		mainCamera.orthographicSize = slider.value * 100f + .01f;
	}

	public void ClientButton(){
		SharedGlobals.instance.StartClient();
	}
	public void ServerButton(){
		SharedGlobals.instance.StartOverview();
	}
}
#endif