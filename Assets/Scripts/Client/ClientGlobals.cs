﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

#if UNITY_WEBGL
	using System.Runtime.InteropServices;
#endif

//This class holds globals only found and used on the client
public class ClientGlobals : MonoBehaviour {	
	#if UNITY_WEBGL
		[DllImport("__Internal")]
    	private static extern string getCookie(string cookieName);
	#endif

	public const int CHAT_LENGTH = 30;

	public List<Message> chatLog;

	public string sessionid;
	public string username;

	public static ClientGlobals instance;

	public Transform debrisContainer;

	public GameObject mainCamera;

	public PlayerInput player = null;

    public Text lbl_userName;

	public int playersOnline;

	public void Start(){
		instance = this;
		Alert_NoFuelColor = Alert_NoFuel.color;
		
		#if UNITY_WEBGL
			sessionid = getCookie("sessionid");
			username = getCookie("username");
		#else
			sessionid = @"$2a$08$9ZBvzsBfrcz/Q5Xm4RWye.";
			username = "";
		#endif
		
		StartCoroutine("UpdateInput");
		StartCoroutine("UpdateHUD");
	}

	public void Update(){
		if (player != null && player.ship != null){

			//Show no fuel alert
			Color tmp = Alert_NoFuel.color;
			if (player.SCRIPT_ship.fuel <= 0){
				tmp.a = 1-Mathf.Abs(Time.time % 2 - 1);
				Alert_NoFuel.color = tmp;
			}
			else{
				tmp.a = 0;
				Alert_NoFuel.color = tmp;
			}
		}

		//Manage Camera
		if (player != null && player.ship != null){
			mainCamera.transform.position = player.ship.transform.position;
		}
	}

	#region InputManager
	bool lastFiredVert = false;//Was our last msg to server a positive vertical axis value?
	bool lastFiredHori = false;//Was our last msg to server a positive horizontal axis value?

	IEnumerator UpdateInput(){
		WaitForSeconds delay = new WaitForSeconds(.01f);
		while (true){
			if (player != null && player.ship != null && !chatInput.isFocused){
				if (Input.GetButtonDown("Fire1")){
					player.CmdFire1();
				}
				if (Input.GetButtonDown("Fire2")){
					player.CmdFire2();
				}
				if (Input.GetButtonDown("Fire3")){
					player.CmdFire3();
				}

				//Impliment some if-not-changed = don't bother the server

				//Get input
				float moveHorizontal = Input.GetAxis("Horizontal");
				float moveVertical = Input.GetAxis("Vertical");

				//Update engine throttle
				if (moveVertical != 0){
					player.CmdForward(moveVertical);
					player.SCRIPT_ship.Forward(moveVertical);
					lastFiredVert = true;
				}
				else if(lastFiredVert){
					player.CmdForward(0);
					player.SCRIPT_ship.Forward(0);
					lastFiredVert = false;
				}

				if (Mathf.Abs(moveHorizontal) != 0){
					player.CmdTilt(moveHorizontal);
					player.SCRIPT_ship.Tilt(-1 * moveHorizontal);
					lastFiredHori = true;
				}
				else if (lastFiredHori){
					player.CmdTilt(0);
					player.SCRIPT_ship.Tilt(0);
					lastFiredHori = false;
				}
			}

			yield return delay;
		}
	}
	#endregion

	#region HUD_Management
	public RectTransform HUD_statsContainer;
	public Text HUD_health;
	public Text HUD_scrap;
	public Text HUD_fuel;
	public Text HUD_theta;
	public Text HUD_radius;
	public Text HUD_usersOnline;
	public RectTransform HUD_healthBar;
	public RectTransform HUD_fuelBar;

	public Text Alert_NoFuel;
	Color Alert_NoFuelColor;

	IEnumerator UpdateHUD(){
		WaitForSeconds delay = new WaitForSeconds(.1f);

		while (true){
			if (player == null || player.ship == null){
				HUD_health.text = "Health: 0";
				HUD_usersOnline.text = "";
			}
			else{
				lbl_userName.text = username;

				HUD_health.text = "Health: " + (Mathf.Round(player.SCRIPT_ship.health * 10) / 10).ToString();
				HUD_scrap.text = "Scrap: " + (Mathf.Round(player.scrap * 10) / 10).ToString();
				HUD_fuel.text = "Fuel: " + (Mathf.Round(player.SCRIPT_ship.fuel * 10) / 10).ToString();
				HUD_theta.text = "Radius: " + (Mathf.Round(player.SCRIPT_ship.Polar()[0] * 10) / 10).ToString();
				HUD_radius.text = "Theta: " + (Mathf.Round(player.SCRIPT_ship.Polar()[1] * 10) / 10).ToString() + "π";
				HUD_usersOnline.text = "Players Online: " + playersOnline;

				//Update Health bar
				HUD_healthBar.anchoredPosition = new Vector3(
					((player.SCRIPT_ship.health / player.SCRIPT_ship.maxHealth) - 1) * 300,
					HUD_healthBar.anchoredPosition.y,
					0
				);

				//Update fuel bar
				HUD_fuelBar.anchoredPosition = new Vector3(
					((player.SCRIPT_ship.fuel / player.SCRIPT_ship.maxFuel) - 1) * 300,
					HUD_fuelBar.anchoredPosition.y,
					0
				);

				if (player.SCRIPT_ship.fuel <= 0){
					Alert_NoFuelColor.a = 1-Mathf.Abs(Time.time % 2 - 1);
					Alert_NoFuel.color = Alert_NoFuelColor;
				}
				else{
					Alert_NoFuelColor.a = 0;
					Alert_NoFuel.color = Alert_NoFuelColor;
				}
			}

			yield return delay;
		}
	}
	#endregion
	
	#region chat
	public TextSystem chatBox;
	public TextField chatInput;

	public const int MAX_MESSAGE_LENGTH = 100;
	public const int MIN_MESSAGE_LENGTH = 5;

	public void ChangedChat(string msg){
		if(player == null){
			chatBox.Enqueue(new Message(null, "You are disconnected from server."));
		}
		if(msg.Length > MAX_MESSAGE_LENGTH){
			chatBox.Enqueue(new Message(null, "Message too long."));
		}
	}

	public void SubmitChat(string msg){
		if(player == null){
			chatBox.Enqueue(new Message(null, "You are disconnected from server."));
		}
		else if(msg.Length < MIN_MESSAGE_LENGTH){
			chatBox.Enqueue(new Message(null, "Message too short."));
		}
		else if(msg.Length > MAX_MESSAGE_LENGTH) {
			chatBox.Enqueue(new Message(null, "Message too long."));
		}
		else{
			player.CmdChatToServer(msg);
		}
	}

	#endregion

	#region GameStartAndEnd
	public GameObject JoinGameMenu;

	public void JoinGame(){
        SharedGlobals.NETWORK.StartClient();
		JoinGameMenu.SetActive(false);
	}

	public const String deathText = "Your crew was lost in the endless abyss that is space...";

	public Text Alert_deathText;
	public Image Btn_playAgain;
	public Text playAgainBtnText;

	//Called by playerinput on ship deassignment
	public void GameOver(){
        SharedGlobals.NETWORK.StopClient();


		StartCoroutine("CreateEndScreen");
	}

	//This coroutine is run on death
	IEnumerator CreateEndScreen(){
		Alert_deathText.text = "";

		//Make button visible
		Color deathTextColor = Alert_deathText.color;
		deathTextColor.a = 1;
		Alert_deathText.color = deathTextColor;

		WaitForSeconds delay = new WaitForSeconds(.0005f);

		//Slowly add to text
		for(int i = 0; i < deathText.Length; i++){
			Alert_deathText.text += deathText[i];
        	yield return delay;
		}

		Btn_playAgain.raycastTarget = true;

		//Make button visible
		Color Btn_playAgainColor = Btn_playAgain.color;
		Btn_playAgainColor.a = 1;
		Btn_playAgain.color = Btn_playAgainColor;

		//Make button text visible
		Color playAgainBtnTextColor = playAgainBtnText.color;
		playAgainBtnTextColor.a = 1;
		playAgainBtnText.color = playAgainBtnTextColor;
	}

	//This is linked to the Play Again button after death
	public void PlayAgain(){
		//Create a new ship
		player.CmdSpawnShip(0, 0);

		//Make text invisible
		Color deathTextColor = Alert_deathText.color;
		deathTextColor.a = 0;
		Alert_deathText.color = deathTextColor;

		//Make button invisible
		Color Btn_playAgainColor = Btn_playAgain.color;
		Btn_playAgainColor.a = 0;
		Btn_playAgain.color = Btn_playAgainColor;

		//Make button text invisible
		Color playAgainBtnTextColor = playAgainBtnText.color;
		playAgainBtnTextColor.a = 0;
		playAgainBtnText.color = playAgainBtnTextColor;

		Btn_playAgain.raycastTarget = false;

		
		JoinGameMenu.SetActive(true);
	}
	#endregion
}
