﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScripts : MonoBehaviour {
	public List<Transform> Pages;
	public Transform PF_ShopEntry;

	int currentPage = 0;

	// Use this for initialization
	void Start () {
		//Populate shop from shop prefabs
		for(int i = SharedGlobals.instance.PF_SHIPS.Count - 1; i != 0; i--){
			for(int k = 0; k < SharedGlobals.instance.PF_SHIPS[i].SHIPS.Count; k++){
				//Create Entry in Shop
				Transform NewEntry = Instantiate(PF_ShopEntry);
				NewEntry.GetChild(2).GetComponent<Text>().text = SharedGlobals.instance.PF_SHIPS[i].SHIPS[k].GetComponent<Ship>().shipName;
				NewEntry.GetChild(8).GetComponent<Image>().sprite = SharedGlobals.instance.PF_SHIPS[i].SHIPS[k].GetComponent<Ship>().sprite;
				//Assign Tab as Parent
				NewEntry.SetParent(Pages[SharedGlobals.instance.PF_SHIPS.Count - 1 - i].GetChild(0).GetChild(0));

				//Assign Transform
				NewEntry.localPosition = new Vector3(
					-225 + 225 * (k % 3),
					225 - 225 * Mathf.Floor(k / 3),
					NewEntry.localPosition.z
				);
				NewEntry.localScale = new Vector3(1f,1f,1f);
				//Enable
				NewEntry.gameObject.SetActive(true);

			}
		}
	}
	
	public void ToggleShop(bool Open){
		this.gameObject.SetActive(Open);
	}

	public void ScrollPage(Transform Page){
		Page.GetChild(0).GetChild(0).localPosition = new Vector3(
			Page.GetChild(0).GetChild(0).localPosition.x,//Sustain current X
			Page.GetChild(1).GetComponent<Scrollbar>().value * 250/*Height of Box*/ * 
			(Mathf.Ceil(Page.GetChild(0).GetChild(0).childCount / 3) - 2.5f),//Number of ships + 1 row
			Page.GetChild(0).GetChild(0).localPosition.z//Sustain current Y
		);
	}

	public void SwapPage(int Page){
		currentPage = Page;

		for(int i = 0; i < Pages.Count; i++){
			Pages[i].gameObject.SetActive(i == Page);
		}
	}

	public void PurchaseShip(int ship){
		ClientGlobals.instance.player.CmdSpawnShip(SharedGlobals.instance.PF_SHIPS.Count - 1 - currentPage, ship);
		ToggleShop(false);
	}
	public void Upgrade(int upgradeType){
		Debug.Log(upgradeType.ToString() + " Upgraded.");
	}
}
