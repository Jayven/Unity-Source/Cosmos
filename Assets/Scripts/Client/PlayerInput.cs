﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using System.Collections;

//This script is shared between client and server
public class PlayerInput : NetworkBehaviour {
	[SyncVar]
	public int killed;

	[SyncVar(hook = "updateUsername")]
	public String username;

	public GameObject ship;
	public Ship SCRIPT_ship;

	[SyncVar]
	public int scrap = 0;

	void Start(){
		if(!isServer){
			if (!isLocalPlayer){
				//If this isn't player's script, destroy it.
				Destroy(this.gameObject);
			}
			else{
				ClientGlobals.instance.player = this;

				//Get sessionid and pass it
				CmdpostNet_init(ClientGlobals.instance.sessionid);
			}
		}
		else{
			ServerGlobals.instance.players.Add(this);

			ServerGlobals.instance.players.ForEach(delegate(PlayerInput playerObject){
				playerObject.RpcPlayerJoin(ServerGlobals.instance.players.Count);
			});
		}
	}

	[Command]
    public void CmdpostNet_init(string sessionid){
		StartCoroutine(postNet_init(sessionid, this));
	}

	[Server]
	public IEnumerator postNet_init(string sessionID, PlayerInput player){
		UnityWebRequest www = UnityWebRequest.Post("https://Jayven.net/getUsername", "");

		www.SetRequestHeader("Cookie", "sessionid=" + sessionID);

        yield return www.SendWebRequest();

        username = www.downloadHandler.text;
			ServerGlobals.instance.Log(username + " joined\t");
			ServerGlobals.instance.Log(ServerGlobals.instance.players.Count + "\tOnline");

		ServerGlobals.instance.SpawnShip(0, 0, this);
    }

	private void updateUsername(string newUsername){
		username = newUsername;
		ClientGlobals.instance.username = newUsername;
	}

	[Command]
	public void CmdSpawnShip(int tier, int shipID){
		ServerGlobals.instance.SpawnShip(tier, shipID, this);
	}

	[ClientRpc]
	public void RpcPlayerJoin(int players){
		ClientGlobals.instance.playersOnline = players;

	}
	[ClientRpc]
	public void RpcPlayerQuit(int players){
		ClientGlobals.instance.playersOnline = players;
	}

	//Assign player vars client side
	[ClientRpc]
	public void RpcAssignShip(GameObject p_ship){
		ship = p_ship;
		SCRIPT_ship = p_ship.GetComponent<Ship>();

		SCRIPT_ship.mapIcon.GetComponent<SpriteRenderer>().color = new Color(0,255,0);
	}

	[ClientRpc]
	public void RpcDeassignShip(){
		ship = null;
		SCRIPT_ship = null;

		ClientGlobals.instance.GameOver();
	}

	#region Inputs
	[Command]
	public void CmdFire1(){
		SCRIPT_ship.Fire1();
	}
	[Command]
	public void CmdFire2(){
		SCRIPT_ship.Fire2();
	}
	[Command]
	public void CmdFire3(){
		SCRIPT_ship.Fire3();
	}
	[Command]
	public void CmdForward(float moveVertical){
		if(SCRIPT_ship != null){
			SCRIPT_ship.Forward(moveVertical);
		}
	}
	[Command]
	public void CmdTilt(float moveHorizontal){
		if(SCRIPT_ship != null){
			SCRIPT_ship.Tilt(-1 * moveHorizontal);
		}
	}
	#endregion

	[Server]
	public void OnDestroy(){
		Destroy(ship);
		if(isServer){
			ServerGlobals.instance.players.Remove(this);
			ServerGlobals.instance.Log(username + " disconnected\t");
			ServerGlobals.instance.Log(ServerGlobals.instance.players.Count + "\tOnline");

			ServerGlobals.instance.players.ForEach(delegate(PlayerInput playerObject){
				playerObject.RpcPlayerQuit(ServerGlobals.instance.players.Count);
			});
		}
	}

	[Server]
	public void KilledSomeone(string p_playerName){
		killed++;
		ServerGlobals.instance.EmitMessage(new Message(null, username + " killed " + p_playerName, new Color(.25f,.25f,.25f)));
		RpcKilled(p_playerName);
	}

	[ClientRpc]
	public void RpcKilled(string p_playername){
		//ClientGlobals.instance.chatBox.Enqueue(new Message(null, "You killed " + p_playername, new Color(.25f,.25f,.25f)));
	}

	[ClientRpc]
	public void RpcChatToClient(Message message){
		ClientGlobals.instance.chatBox.Enqueue(message);
	}

	[Command]
	public void CmdChatToServer(string message){
		ServerGlobals.instance.EmitMessage(new Message(username, new Color(.25f,.25f,.25f), message, new Color(.75f,0f,0f)));
	}
	[ClientRpc]
	public void RpcDied(string p_playername){
		ClientGlobals.instance.chatBox.Enqueue(new Message(null, "You were destroyed by " + p_playername));
	}
}
