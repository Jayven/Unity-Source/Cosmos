﻿using System;
 
public static class Epoch  {
    static DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
 
	public static double Current(){
		return Math.Round((DateTime.UtcNow - epochStart).TotalSeconds);
	}
}