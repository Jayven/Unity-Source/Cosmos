using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

class ServerGlobals : MonoBehaviour{
    public static ServerGlobals instance;

    public List<PlayerInput> players = new List<PlayerInput>();

    public List<Ship> playerShips = new List<Ship>();//Holds every player's ship
    public List<Ship> cpuShips = new List<Ship>();//Holds all computer's ships

    public GameObject[] PF_smallDebriss = null;
    public GameObject[] PF_debriss = null;
    public GameObject[] PF_largeDebriss = null;
    List<List<GameObject>> debriss = new List<List<GameObject>>();

    public const float playerSpace = 1;//space on map for each player
    public const int maxPlayers = 50;//maximum players the map prepares space for, not max server can possibly hold

    void Start(){
        SetupFileIO();
        Log("Starting Server");
        instance = this;
        SharedGlobals.NETWORK.StartServer();

        SpawnAsteroids();
        StartCoroutine(Maintain());
        Log("    Port: " + SharedGlobals.NETWORK.networkPort.ToString());
        Log("    Address: " + SharedGlobals.NETWORK.networkAddress.ToString());
        Log("    Use websocket: " + (SharedGlobals.NETWORK.useWebSockets? "Yes": "No"));
        Log("Server Started");
    }

    IEnumerator Maintain(){
		WaitForSeconds delay = new WaitForSeconds(5f);
        while (true){
            if (playerShips.Count > 0){
                //Spawn new cpu
                while(cpuShips.Count < Mathf.Round(100 / (playerShips.Count + 5)) + 5){
                    SpawnCPU(Random.Range(0, SharedGlobals.instance.PF_SHIPS[0].SHIPS.Count));
                }
            }
            else{
                //If no players, destroy all cpu's to save power
                foreach(Ship ship in cpuShips){
                    Destroy(ship.gameObject);
                }
            }

            yield return delay;
        }
    }

    void SpawnAsteroids(){
        for (int r = 0; r < maxPlayers * playerSpace * 2; r++){
            debriss.Add(new List<GameObject>());

            float clustered;//Used to determine astroid size and quantity

            if (r < maxPlayers * playerSpace){
                clustered = r;
            }
            else{
                clustered = (maxPlayers * playerSpace) - (r - (maxPlayers * playerSpace));
            }
            
            //Still inside Universe rim
            for (int n = 0; n < clustered / 8; n++){
                GameObject debris = Instantiate(
                    PF_debriss[Random.Range(0, PF_debriss.Count())],
                    Polar2Cart(Random.Range(r, r-1), Random.Range(0f, 360f)),
                    Quaternion.Euler(RandRot())
                );

                float x = Random.Range(.5f, (float)clustered / 8f);
                float y = Random.Range(.5f, (float)clustered / 8f);

                debris.transform.localScale = new Vector3(
                    x,
                    y,
                    0
                );

                debris.GetComponent<debris>().scale = new Vector2(x, y);

                debriss.Last().Add(debris);

                NetworkServer.Spawn(debris);
            }
        }

        Log((maxPlayers * playerSpace * 2).ToString() + " Debriss spawned.");
    }

    Vector3 Polar2Cart(float r, float theta){
        return new Vector3(
            r * Mathf.Cos(theta),
            r * Mathf.Sin(theta),
            0
        );
    }

    Vector3 RandRot(){
        return new Vector3(
            0,
            0,
            Random.Range(0f, 360f)
        );
    }

    public void SpawnCPU(int shipID){

        Vector3 trans = new Vector3(
            Random.Range(- maxPlayers * playerSpace * 2f, maxPlayers * playerSpace * 2f),
            Random.Range(- maxPlayers * playerSpace * 2f, maxPlayers * playerSpace * 2f),
            0
        );

       	GameObject ship = (GameObject)Instantiate(SharedGlobals.instance.PF_SHIPS[0].SHIPS[shipID], trans, Quaternion.Euler(RandRot()));
		
        ship.GetComponent<Ship>().init(null);
    }

    public void SpawnShip(int tier, int shipID, PlayerInput player){
		if (shipID < SharedGlobals.instance.PF_SHIPS[tier].SHIPS.Count && player.scrap >= SharedGlobals.instance.PF_SHIPS[tier].SHIPS[shipID].GetComponent<Ship>().cost){
			Transform shipTrans = null;

			//This means the previous ship was not destroyed, we're replacing it
			if (player.ship != null){
				//Store the previous ship's position
				shipTrans = player.ship.transform;
			
				//Set ship's reference to player to null to avoid it auto respawning player
				player.SCRIPT_ship.player = null;
				
				//Awkward set ship to null before destroying object to avoid detached ship state in other threads
				GameObject tmpShip = player.ship;
				player.ship = null;

				Destroy(tmpShip);
			}

			if (shipTrans != null){
				//Use the previous position if it existed
				player.ship = Instantiate(SharedGlobals.instance.PF_SHIPS[tier].SHIPS[shipID], shipTrans.position, shipTrans.rotation);
			}
			else{
				//Otherwise, throw it back to the origin
				player.ship = Instantiate(
                    SharedGlobals.instance.PF_SHIPS[tier].SHIPS[shipID],
                    new Vector3(
                        Random.Range(-25f, 25f),
                        Random.Range(-25f, 25f),
                        0f
                    ),
                    Quaternion.Euler(RandRot())
                );
			}

			player.SCRIPT_ship = player.ship.GetComponent<Ship>();
			player.SCRIPT_ship.init(player);
			
			player.RpcAssignShip(player.ship);
		}
	}

    public void EmitMessage(Message Message){
        LogChat(Message);
        players.ForEach(delegate(PlayerInput playerObject){
            playerObject.RpcChatToClient(Message);
        });
    }

    public void SetupFileIO(){
        if (!File.Exists("chat.log")){
            using (StreamWriter writer = File.CreateText("chat.log")) {}	
        }
        if (!File.Exists("main.log")){
            using (StreamWriter writer = File.CreateText("main.log")) {}	
        }
    }

    public void LogChat(Message chatMessage){
        using (StreamWriter writer = File.AppendText("chat.log")) {
            writer.WriteLine(
                Epoch.Current().ToString() + "\t" + 
                chatMessage.senderName + "\t" + 
                chatMessage.message
            );
        }
    }

    public void Log(string data){
        using (StreamWriter writer = File.AppendText("main.log")) {
            writer.WriteLine(
                Epoch.Current().ToString() + "\t" + 
                data
            );
        }
    }
}