﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Tier{
	public List<GameObject> SHIPS;
}

public class SharedGlobals : MonoBehaviour {
	public static SharedGlobals instance;
	public static NetworkManager NETWORK;
	//[List of tier 0, List of tier 1, List of tier 2, List of tier 3]
	public List<Tier> PF_SHIPS;


	public void Start(){
		instance = this;
		NETWORK = this.gameObject.GetComponent<NetworkManager>();

		#if SERVER
			StartServer();
		#elif CLIENT
			StartClient();
		#else
			SceneManager.LoadScene("DEV-Start", LoadSceneMode.Single);
		#endif

		//Setup Network connection properties
		#if UNITY_WEBGL
			NETWORK.useWebSockets = true;
			NETWORK.networkAddress = "wss://Jayven.net";
			NETWORK.networkPort = 45001;
		#elif SERVER
			NETWORK.useWebSockets = true;
			NETWORK.networkAddress = "*";
			NETWORK.networkPort = 40001;
		#else
			NETWORK.useWebSockets = false;
			NETWORK.networkAddress = "localhost";
			NETWORK.networkPort = 40001;
		#endif
	}

	#if !CLIENT
		public void StartServer(){
			SceneManager.LoadScene("Server", LoadSceneMode.Single);
		}
	#endif

	#if !SERVER
		public void StartClient(){
			SceneManager.LoadScene("Client", LoadSceneMode.Single);
		}
	#endif

	#if !(SERVER || CLIENT)
		public void StartOverview(){
			SceneManager.LoadScene("Server-WithOverview", LoadSceneMode.Single);
			SceneManager.LoadScene("Server", LoadSceneMode.Additive);
		}
	#endif
}

public class DamageClass{
	public DamageClass(float p_damage, PlayerInput p_damager){
		damage = p_damage;
		damager = p_damager;
	}
	public float damage;
	public PlayerInput damager;
}

[System.Serializable]
public class Message{
	public String senderName = null;
	public Color senderColor = new Color(.25f,.25f,.25f);
	public String message = null;
	public Color messageColor = Color.red;

	public Message(){}
	public Message(String p_sender, Color p_senderColor, String p_message, Color p_messageColor){
		senderName = p_sender;
		senderColor = p_senderColor;
		message = p_message;
		messageColor = p_messageColor;
	}

	public Message(String p_sender, String p_message, Color p_messageColor){
		senderName = p_sender;
		message = p_message;
		messageColor = p_messageColor;
	}

	public Message(String p_sender, String p_message){
		senderName = p_sender;
		message = p_message;
	}

	public Message(String p_message){
		message = p_message;
	}
}