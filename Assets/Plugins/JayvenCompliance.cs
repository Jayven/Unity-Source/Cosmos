﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Contains code that should be held by all games on Jayven
public class JayvenCompliance : MonoBehaviour{
	public void Start() {
		//Allows keyboard input on rest of site
		#if !UNITY_EDITOR && UNITY_WEBGL
			WebGLInput.captureAllKeyboardInput = false;
		#endif
	}
}
