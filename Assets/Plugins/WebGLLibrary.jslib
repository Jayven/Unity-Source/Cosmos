﻿mergeInto(LibraryManager.library, {
	getCookie: function (a) {
		var b = document.cookie.match('(^|;)\\s*' + Pointer_stringify(a) + '\\s*=\\s*([^;]+)');
		returnStr =  decodeURIComponent(b ? b.pop() : '');

		var bufferSize = lengthBytesUTF8(returnStr) + 1;
		var buffer = _malloc(bufferSize);
		stringToUTF8(returnStr, buffer, bufferSize);

		return buffer;
	},
});
