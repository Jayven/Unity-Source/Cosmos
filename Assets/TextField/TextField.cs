﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(Button))]

public class TextField : MonoBehaviour {
	public bool isFocused = false;

	public Color focusedColor;
	public Color unfocusedColor;

	public Button submitButton;

	private Text textbox;
	
    public StrEvent OnSubmit;
	public StrEvent OnChanged;

	void Start () {
		textbox = this.GetComponent<Text>();
		submitButton.onClick.AddListener(() => Submit());
		this.GetComponent<Button>().onClick.AddListener(() => EnterField());
	}
	
	void Update () {
		if(isFocused){
			foreach (char c in Input.inputString){
				if (c == '\b'){ // has backspace/delete been pressed?
					if (textbox.text.Length > 0){
						textbox.text = textbox.text.Substring(0, textbox.text.Length - 1);
					}
					OnChanged.Invoke(textbox.text);
				}
				else if ((c == '\n') || (c == '\r')){ // enter/return
					Submit();
				}
				else{
					textbox.text += c;
					OnChanged.Invoke(textbox.text);
				}
			}
		}
		else{
			if(Input.GetButtonDown("Submit")){
				EnterField();
			}
		}
	}
	public void EnterField(){
		isFocused = true;
		textbox.text = "";
		textbox.color = focusedColor;
		submitButton.gameObject.SetActive(true);
	}

	public void Submit(){

		OnSubmit.Invoke(textbox.text);
		Cancel();
	}

	public void Cancel(){
		textbox.text = "Press enter to chat...";
		isFocused = false;
		textbox.color = unfocusedColor;
		submitButton.gameObject.SetActive(false);
	}

}

[System.Serializable]
public class StrEvent : UnityEvent<string>{}